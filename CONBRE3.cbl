       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CONBRE3.
       AUTHOR. Fhanglerr

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT 100-INPUT-FILE ASSIGN TO "DATA3.DAT"
              ORGANIZATION IS LINE SEQUENTIAL
              FILE STATUS IS WS-INPUT-FILE-STATUS.
           SELECT 200-OUTPUT-FILE ASSIGN TO "REPORT3.RPT"
              ORGANIZATION IS LINE SEQUENTIAL
              FILE STATUS IS WS-OUTPUT-FILE-STATUS.

       DATA DIVISION. 
       FILE SECTION. 
       FD 100-INPUT-FILE
           BLOCK CONTAINS 0 RECORDS.
       01 INPUT-FILE-RECORD.
          05 BRANCH-ID            PIC X.
          05 FILLER               PIC X(6).
          05 DATE-DMY             PIC X(8).
          05 FILLER               PIC X(3).
          05 PRODUCT              PIC X(9).
          05 INCOME               PIC 9(3).
       FD 200-OUTPUT-FILE
           BLOCK CONTAINS 0 RECORDS.
       01 OUTPUT-FILE-RECORD      PIC X(37).

       WORKING-STORAGE SECTION. 
       01 WS-INPUT-FILE-STATUS    PIC X(2).
          88 FILE-OK                         VALUE "00".
          88 FILE-AT-END                     VALUE "10".
       01 WS-OUTPUT-FILE-STATUS   PIC X(2).
          88 FILE-OK                         VALUE "00".
          88 FILE-AT-END                     VALUE "10".
       01 WS-CACULATION.
          05 WS-READ-COUNT-INPUT  PIC 9(5)   VALUE ZEROS.
          05 WS-ALL-TOTAL         PIC 9(5).
          05 WS-BRACH-TOTAL       PIC 9(5).
          05 WS-BRACH-CB          PIC X.
          05 WS-BRACH-SHOW        PIC X      VALUE "S".
          05 WS-DATE-TOTAL        PIC 9(5).
          05 WS-DATE-CB           PIC X(8).
          05 WS-DATE-SHOW         PIC X      VALUE "S".
          05 WS-PRODUCT-TOTAL     PIC 9(5).
          05 WS-PRODUCT-CB        PIC X(9).
       01 RPT-FORMAT.
          05 RPT-HEADER           PIC X(37)
                                             VALUE
                "BRANCH   DATE   PRODUCT        INCOME".
          05 RPT-DETAIL.
             10 RPT-BRANCH-ID     PIC X.
             10 FILLER            PIC X(6)   VALUE SPACES.
             10 RPT-DATE          PIC X(8).
             10 FILLER            PIC X(2)   VALUE SPACES.
             10 RPT-PRODUCT       PIC X(9).
             10 FILLER            PIC X(6)   VALUE SPACES.
             10 RPT-TOTAL         PIC ZZZZZ9.
          05 RPT-DATE-TOTAL.
             10 FILLER            PIC X(32)
                                             VALUE
                   "            DATE TOTAL          ".
             10 RPT-TOTAL         PIC ZZZZZ9.
          05 RPT-BRANCH-TOTAL.
             10 FILLER            PIC X(32)
                                             VALUE
                   "----------BRANCH TOTAL----------".
             10 RPT-TOTAL         PIC ZZZZZ9.
          05 RPT-ALL-TOTAL.
             10 FILLER            PIC X(32)
                                             VALUE
                   ">          ALL TOTAL           <".
             10 RPT-TOTAL         PIC ZZZZZ9.

          
       PROCEDURE DIVISION.
       0000-MAIN-PROGRAM.
           PERFORM 1000-INITIAL THRU 1000-EXIT
           PERFORM 2000-PROCESS THRU 2000-EXIT
              UNTIL FILE-AT-END OF WS-INPUT-FILE-STATUS 
           PERFORM 3000-END THRU 3000-EXIT 
           GOBACK
           .
       1000-INITIAL.
           OPEN INPUT 100-INPUT-FILE
           OPEN OUTPUT 200-OUTPUT-FILE 
           MOVE ZEROS TO WS-ALL-TOTAL 
           MOVE RPT-HEADER TO OUTPUT-FILE-RECORD 
           PERFORM 7000-WRITE THRU 7000-EXIT
           PERFORM 8000-READ THRU 8000-EXIT
           .
       1000-EXIT.
           EXIT.

       2000-PROCESS.
           MOVE BRANCH-ID TO WS-BRACH-CB 
           MOVE ZEROS TO WS-BRACH-TOTAL 
           PERFORM 2100-BRANCH-PROCESS THRU 2100-EXIT
              UNTIL FILE-AT-END OF WS-INPUT-FILE-STATUS
              OR BRANCH-ID NOT = WS-BRACH-CB
           MOVE WS-BRACH-TOTAL TO RPT-TOTAL OF RPT-BRANCH-TOTAL 
           DISPLAY RPT-BRANCH-TOTAL
           MOVE "S" TO WS-BRACH-SHOW 

           .
       2000-EXIT.
           EXIT.
       2100-BRANCH-PROCESS.
           MOVE DATE-DMY TO WS-DATE-CB  
           MOVE ZEROS TO WS-DATE-TOTAL  
           PERFORM 2200-DATE-PROCESS THRU 2200-EXIT
              UNTIL FILE-AT-END OF WS-INPUT-FILE-STATUS
              OR BRANCH-ID NOT = WS-BRACH-CB
              OR DATE-DMY NOT = WS-DATE-CB
           MOVE WS-BRACH-TOTAL TO RPT-TOTAL OF RPT-DATE-TOTAL 
           MOVE RPT-BRANCH-TOTAL TO OUTPUT-FILE-RECORD            
           PERFORM 7000-WRITE THRU 7000-EXIT
           MOVE "S" TO WS-DATE-SHOW

           .
       2100-EXIT.
           EXIT.
       2200-DATE-PROCESS.
           MOVE PRODUCT TO WS-PRODUCT-CB   
           MOVE ZEROS TO WS-PRODUCT-TOTAL   
           PERFORM 2300-PRODUCT-PROCESS THRU 2300-EXIT
              UNTIL FILE-AT-END OF WS-INPUT-FILE-STATUS
              OR BRANCH-ID NOT = WS-BRACH-CB
              OR DATE-DMY NOT = WS-DATE-CB
              OR PRODUCT NOT = WS-PRODUCT-CB
           IF WS-BRACH-SHOW = "S"
              MOVE WS-BRACH-CB TO RPT-BRANCH-ID OF RPT-DETAIL
              MOVE "H" TO WS-BRACH-SHOW  
           ELSE
              MOVE SPACES TO RPT-BRANCH-ID OF RPT-DETAIL
           END-IF
           IF WS-DATE-SHOW = "S"
              MOVE WS-DATE-CB TO RPT-DATE OF RPT-DETAIL 
              MOVE "H" TO WS-DATE-SHOW  
           ELSE
              MOVE SPACES TO RPT-DATE OF RPT-DETAIL 
           END-IF
           MOVE WS-PRODUCT-CB TO RPT-PRODUCT OF RPT-DETAIL 
           MOVE WS-PRODUCT-TOTAL TO RPT-TOTAL OF RPT-DETAIL  
           MOVE RPT-DETAIL TO OUTPUT-FILE-RECORD 
           PERFORM 7000-WRITE THRU 7000-EXIT
           DISPLAY RPT-DETAIL  
 
           .
       2200-EXIT.
           EXIT.
       2300-PRODUCT-PROCESS.
           ADD INCOME TO WS-ALL-TOTAL 
           ADD INCOME TO WS-BRACH-TOTAL 
           ADD INCOME TO WS-DATE-TOTAL 
           ADD INCOME TO WS-PRODUCT-TOTAL 
           PERFORM 8000-READ THRU 8000-EXIT

           .
       2300-EXIT.
           EXIT.
       3000-END.
           MOVE WS-ALL-TOTAL TO RPT-TOTAL OF RPT-ALL-TOTAL 
           MOVE RPT-ALL-TOTAL TO OUTPUT-FILE-RECORD 
           PERFORM 7000-WRITE THRU 7000-EXIT
           DISPLAY "READ: " WS-READ-COUNT-INPUT " RECORDS" 
           CLOSE 100-INPUT-FILE 200-OUTPUT-FILE 
           .
       3000-EXIT.
           EXIT.
           
       7000-WRITE.
           WRITE OUTPUT-FILE-RECORD 
           .
       7000-EXIT.
           EXIT.
       
       8000-READ.
           READ 100-INPUT-FILE 
           ADD 1 TO WS-READ-COUNT-INPUT 
           .
       8000-EXIT.
           EXIT.